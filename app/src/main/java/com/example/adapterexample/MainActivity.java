package com.example.adapterexample;

import android.os.Bundle;

import com.example.adapterexample.App.App;
import com.example.adapterexample.data.db.AppDataBase;
import com.example.adapterexample.data.db.CounterEntity;
import com.example.adapterexample.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements Contract {
    private ActivityMainBinding binding;
    private Adapter adapter;
    List<CounterEntity> list = new ArrayList<>();
    private CompositeDisposable disposable = new CompositeDisposable();
    private AppDataBase db = App.getInstance().getDatabase();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setEvent(this);

        binding.rcv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new Adapter(this);
        binding.rcv.setAdapter(adapter);

        binding.button.setOnClickListener(v -> onClick());
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                adapter.removeItem(viewHolder.getAdapterPosition());
            }

        });
        itemTouchHelper.attachToRecyclerView(binding.rcv);

        disposable.add(db.counterDao().getAll()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<CounterEntity>>() {
                               @Override

                               public void accept(List<CounterEntity> counterEntities) throws Exception {

                                   list.clear();
                                   list.addAll(counterEntities);
                                   adapter.notifyDataSetChanged();
                               }
                           }, new Consumer<Throwable>() {
                               @Override
                               public void accept(Throwable throwable) throws Exception {

                               }
                           }
                ));
    }




    @Override
    public void onClick() {
        adapter.addItem();

    }

    @Override
    public void onLongClick(int i) {
        adapter.updateItem(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
