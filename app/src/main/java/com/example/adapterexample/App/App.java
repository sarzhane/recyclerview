package com.example.adapterexample.App;

import android.app.Application;

import com.example.adapterexample.data.db.AppDataBase;

import androidx.room.Room;

public class App extends Application {

    public static App instance;

    private AppDataBase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, AppDataBase.class, "dataBase")
                .build();
    }

    public static App getInstance() {
        return instance;
    }

    public AppDataBase getDatabase() {
        return database;
    }
}
