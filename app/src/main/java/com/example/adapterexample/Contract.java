package com.example.adapterexample;

public interface Contract {
    void onClick();
    void onLongClick(int i);
}
