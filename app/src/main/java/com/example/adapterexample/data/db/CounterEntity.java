package com.example.adapterexample.data.db;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class CounterEntity {

    @PrimaryKey(autoGenerate = true)
    public long id;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Ignore
    public CounterEntity() {
        count = 0;
    }

    public CounterEntity( int count) {
        this.count = count;

    }
}
