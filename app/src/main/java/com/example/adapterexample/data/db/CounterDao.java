package com.example.adapterexample.data.db;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Flowable;

@Dao
public interface CounterDao {
    @Query("SELECT * FROM counterentity")
   Flowable <List<CounterEntity>> getAll();

    @Query("SELECT * FROM counterentity WHERE id ==:id")
    CounterEntity getById(long id);

    @Insert
    void insert(CounterEntity Count);

    @Update
    void update(CounterEntity count);

    @Delete
    void delete(CounterEntity count);

}
