package com.example.adapterexample.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {CounterEntity.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {
    public abstract CounterDao counterDao();


}
