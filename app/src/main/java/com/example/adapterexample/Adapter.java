package com.example.adapterexample;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.adapterexample.App.App;
import com.example.adapterexample.data.db.AppDataBase;
import com.example.adapterexample.data.db.CounterEntity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class Adapter extends RecyclerView.Adapter<ViewHolder> {
    List<CounterEntity> list;
    private Contract contract;
    private AppDataBase db = App.getInstance().getDatabase();



    public Adapter(Contract contract) {
        this.contract = contract;
        list = new ArrayList<>();
//        list.addAll(db.counterDao().getAll());

    }

    public void addItem() {
        CounterEntity item = new CounterEntity(list.size() + 1);
        list.add(item);
        db.counterDao().insert(item);
        notifyItemInserted(list.size() - 1);

    }

    public void removeItem(int position) {
        CounterEntity counterEntity = list.get(position);
        list.remove(position);
        db.counterDao().delete(counterEntity);
        notifyItemRemoved(position);
    }

    public void updateItem(int position) {
        CounterEntity counterEntity = list.get(position);
        counterEntity.setCount(7777);
        list.set(position, counterEntity);
        db.counterDao().update(counterEntity);
        notifyItemChanged(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false), contract);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
