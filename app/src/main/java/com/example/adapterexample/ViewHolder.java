package com.example.adapterexample;

        import android.view.View;

        import com.example.adapterexample.data.db.CounterEntity;
        import com.example.adapterexample.databinding.ItemMainBinding;

        import androidx.annotation.NonNull;
        import androidx.databinding.DataBindingUtil;
        import androidx.recyclerview.widget.RecyclerView;

public class ViewHolder extends RecyclerView.ViewHolder {
    private ItemMainBinding binding;
    private Contract listner;

    public ViewHolder(@NonNull View itemView, Contract listner) {
        super(itemView);
        this.listner = listner;
        binding = DataBindingUtil.bind(itemView);
        itemView.setOnLongClickListener(v -> {
            listner.onLongClick(getAdapterPosition());
            return true;
        });

    }

    public  void bind(CounterEntity item){
        binding.textView.setText(String.valueOf(item));
    }
}
